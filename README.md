#Beer collection

I have used create-react-app to easily get started, since I have not tried that before, and it seemed pretty straight forward. ([Create React App](https://github.com/facebook/create-react-app).)  
The app is in a _very_ simple POC state, and several things have not been made.  
The app is capable of adding and deleting items. Editing has not been made. The react state is stored in localstorage.  
The user fills out a name and a note for a beer, and data from punkapi is added to the mix.  
A can or bottle .png is added to each item. svg format could have been used here instead. 

Polyfills for IE11 has not been added, hence the app won't work in IE. I have only developed with Chrome in mind at this point.  
Some accessibility thoughts have been made, but has not been main focus.   
A loading state could be added, when fetching data, but has not been implemented yet.  
Linting has not been super strict, and you might find missing semicolons.

React is still quite new to me, which means I do not know the best practices in React - which probably shows. I've got a working POC though, with simple functionality.
Keys on lists seems very important to React, which is why you might find odd keys on eg. description list.  
getBeer() and getImage() are currently in App.js, but could be moved, to declutter the file.

A working version can be seen by cloning this repo, and runnning `npm install`, then run `npm start` for dev mode.  
You can also compile it by running `npm run build`. This creates a build folder, which can then be served locally. (React suggest `npm install -g serve` then `serve -s build`)  
EDIT: I have added build folder to git (I know I know..), so no install needs to be made.