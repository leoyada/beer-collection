import React from 'react';
import './add.scss';

function Add(props) {

    const handleSubmit = (event) => {
        event.preventDefault();

        const form = event.currentTarget;
        const formData = new FormData(form);

        form.reset();

        const productInfo = {
            name: formData.get("name"),
            notes: formData.get("notes")
        };

        props.functionAddProduct(productInfo);
    };

    const handleAddActivate = () => {
        props.functionActivateAddProduct();
    };

    const classNameForAdd = `add product ${props.isToggleActive ? "add--active" : ""}`;

    return (
        <form className={classNameForAdd} onSubmit={handleSubmit} autoComplete="off">
            <button type="button" className="add__toggle" onClick={handleAddActivate}></button>
            <div className="add__content">
                <h3 className="add__headline">Add beer</h3>
                <label className="add__label">
                    <span className="add__label-text">name:</span>
                    <input size="1" className="add__input" name="name" placeholder="Name" type="text" required />
                </label>
                <label className="add__label">
                    <span className="add__label-text">type:</span>
                    <input size="1" className="add__input" name="notes" placeholder="Notes" type="text" required />
                </label>
                <button className="add__submit-btn" type="submit">Add beer!</button>
            </div>
        </form>
    );
}

export default Add;
