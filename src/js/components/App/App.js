import React from 'react';
import Product from '../Product/Product'
import Add from "../Add/Add";
import './app.scss';
import * as testData from '../../../test-data';

function getBeer() {
    return fetch("https://api.punkapi.com/v2/beers/random").then(response => {
        if (response.ok) {
            return response.json();
        } else {
            return Promise.reject({status: response.status});
         }
    }).catch(error => {
        // handleError here
        console.log(error);
    });
}

// Since images from punkapi can be weird, lets use these selfmades for now
function getImage() {
    const rand = Math.round(Math.random());
    const imgArray = ["bottle", "can"];

    // Lets assume every image is png, because, well, I've made them as such
    return `img/${imgArray[rand]}.png`;
}

class App extends React.Component {

    state = {
        products: [],
        activeProductId: 0,
        isToggleActive: false
    };

    addNewProduct(productInfo) {
        const product = productInfo;
        product.id = new Date().getTime().toString(); // Lets just do this for now
        product.image = getImage();

        getBeer().then(data => {
            const beerItem = data[0];

            if (beerItem.abv) product.abv = data[0].abv;
            if (beerItem.description) product.description = data[0].description;
            if (beerItem.ingredients) product.ingredients = data[0].ingredients;

            this.setState({ products: [...this.state.products, product ], isToggleActive: false});
        });
    }

    activateAddProduct() {
        this.setState({ isToggleActive: true, activeProductId: 0 });
    }

    toggleDetailedView(id) {
        if (id === this.state.activeProductId) {
            this.setState({ activeProductId: 0 });
        } else {
            this.setState({ activeProductId: id });
        }
    }

    deleteProduct(id) {
        const productState = this.state.products.filter(test => test.id !== id);

        this.setState({ products: productState, activeProductId: 0 });
    }

    componentDidMount() {
        //if local storage has data, use that
        const localData = localStorage.getItem("beers");

        if (localData !== null) {
            this.setState({ products: JSON.parse(localData)});
        } else {
            this.setState(testData.default);
        }
    }

    componentDidUpdate() {
        if (this.state.products.length < 1) {
            localStorage.removeItem("beers");
        } else {
            localStorage.setItem("beers", JSON.stringify(this.state.products));
        }
    }

    render() {
        return (
            <div className="app">
                <header className="app__header">
                    <h1 className="app__headline">My very own beer collection</h1>
                </header>
                <div className="app__product-container">
                    {this.state.products.map(product =>
                        <Product
                            key={product.id}
                            product={product}
                            activeProductId={this.state.activeProductId}
                            functionToggleDetailedView={id => this.toggleDetailedView(id)}
                            functionDeleteProduct={id => this.deleteProduct(id)}
                        />
                    )}
                    <Add
                        isToggleActive={this.state.isToggleActive}
                        functionAddProduct={productInfo => this.addNewProduct(productInfo)}
                        functionActivateAddProduct={() => this.activateAddProduct()}
                    />
                </div>
            </div>
        );
    }
}

export default App;
