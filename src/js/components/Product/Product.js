import React from 'react';
import './product.scss';
import ProductIngredients from "../ProductIngredients/ProductIngredients";

function Product(props) {
    const {image, name, notes, abv, id, description, ingredients} = props.product;

    const handleProductClick = () => {
        props.functionToggleDetailedView(id);
    };

    const handleDeleteClick = () => {
        props.functionDeleteProduct(id);
    };

    const productClassName = `product ${props.activeProductId === id ? "product--open" : props.activeProductId !== 0 ? "product--faded" : ""}`;

    return (
        <div className={productClassName}>
            <div className="product__content">
                <div className="product__image-wrapper">
                    <img className="product__image" src={image} alt={name} />
                </div>
                <div className="product__info">
                    <h3 className="product__name">{name}</h3>
                    <p className="product__type">{notes}</p>
                    {abv ? <p className="product__abv">{abv}%</p> : null}
                </div>
                <div className="product__extended-info">
                    {description ? <p className="product__description">{description}</p> : null}
                    {ingredients ? <ProductIngredients key={id + "ingredients"} ingredients={ingredients} parentId={id} /> : null}
                </div>
            </div>
            <button type="button" className="product__details-btn" aria-label="Click here to open detailed view" onClick={handleProductClick}></button>
            <button className="product__delete-btn" type="button" aria-label="Click to delete product" onClick={handleDeleteClick}></button>
        </div>
    );
}

export default Product;
