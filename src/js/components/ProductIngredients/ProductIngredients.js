import React from 'react';
import './ProductIngredients.scss';


// Makes object in the format we want, from the data provided by punkapi
function getIngredientObject(data) {
    return Object.entries(data).map(ingredient => {
        let arrayOfIngredients = [];
        const mainIngredient = ingredient[0];

        if (Array.isArray(ingredient[1])) {
            arrayOfIngredients.push(...ingredient[1].map( ingredientDetail => {
                return ingredientDetail.name;
            }));
        } else {
            arrayOfIngredients.push(ingredient[0]);
        }

        arrayOfIngredients = [...new Set(arrayOfIngredients)]; // We only want unique values

        return {descriptor: mainIngredient, arrayOfDetails: arrayOfIngredients};
    });
}


function ProductIngredients(props) {
    const {ingredients, parentId} = props;
    const ingredientsData = getIngredientObject(ingredients);

    return (
        <div className="product-ingredients__container">
            <h4 className="product-ingredients__headline">Ingredients</h4>
            <dl key={parentId + "dl"} className="product-ingredients">
                {ingredientsData.map((ingredient, index) => {
                    return (
                        <div key={parentId + index + "description-wrapper"} className="product-ingredients__description-wrapper">
                            <dt key={parentId + index + "dt"}
                                className="product-ingredients__descriptor">{ingredient.descriptor}:</dt>
                            {
                                ingredient.arrayOfDetails.map((data, index) => {
                                    return (<dd
                                        key={parentId + index + "dd"}
                                        className="product-ingredients__detail">{data + (index === ingredient.arrayOfDetails.length - 1 ? "" : ", ")}
                                    </dd>)
                                })
                            }
                        </div>
                    );
                })}
            </dl>
        </div>
    );
}

export default ProductIngredients;
